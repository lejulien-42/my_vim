#!/bin/sh
# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    install.sh                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lejulien <lejulien@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/07/28 18:28:44 by lejulien          #+#    #+#              #
#    Updated: 2020/07/28 18:29:15 by lejulien         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

rm -fr ~/.vim
rm ~/.vimrc
rm ~/.zshrc
rm -fr ~/.oh-my-zsh

sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"


cp -fr vim ~/.vim
cp vimrc ~/.vimrc
cp zshrc ~/.zshrc
echo "export MAIL=\"lejulien@student.42.fr\"" >> ~/.zshrc
source ~/.zshrc
